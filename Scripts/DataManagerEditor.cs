using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DataManager))]
public class DataManagerEditor : Editor
{
    private string recordButtonText;

    public override void OnInspectorGUI() {

        DataManager dm = (DataManager)target;

        if(dm.record) {
            recordButtonText = "Stop Recording";
            GUI.color = Color.red;
        } else {
            recordButtonText = "Start Recording";
            GUI.color = Color.green;
        }

        base.OnInspectorGUI();

        var style = new GUIStyle(GUI.skin.button);

        style.normal.textColor = Color.white;

        if(GUILayout.Button(recordButtonText, style))
        { 
            Debug.Log(recordButtonText);
            dm.record = !dm.record;
        }

        if(GUILayout.Button("Save Data", style)) {
            Debug.Log("Saving data. Waiting for end of recording.");
            dm.save = true;
        }

        if(GUILayout.Button("Clear Data", style)) {
            Debug.Log("Clearing data. Waiting for end of recording.");
            dm.clear = true;
        }
    }
}
