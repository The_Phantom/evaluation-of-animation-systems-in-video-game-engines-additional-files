using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataManager : MonoBehaviour
{
    [HideInInspector]
    public bool record = false;

    [HideInInspector]
    public bool save = false;

    [HideInInspector]
    public bool clear = false;

    [SerializeField]
    private List<JointDataGatherer> JointDataGatheres;

    [SerializeField]
    private string savePath = "/Saves/Save";


    public List<DataFrame> dataFrames;

    private void Start() {
        dataFrames = new List<DataFrame>();
    }

    void Update()
    {
        if(record)
        {
            CreateDataFrame();
        }
        else if(save)
        { 
            Save();
            save = false;
        }
        else if(clear)
        { 
            Clear();
            clear = false;
        }
    }

    private void CreateDataFrame()
    { 
        DataFrame dataFrame = new DataFrame();

        dataFrame.dataFrameID = dataFrames.Count + 1;
        dataFrame.engineFramecount = Time.frameCount;
        dataFrame.frameTime = Time.deltaTime;        

        dataFrame.JointsData = new List<JointData>();
        AddJointDataToDataFrame(dataFrame);

        dataFrames.Add(dataFrame);
    }

    private void AddJointDataToDataFrame(DataFrame dataFrame)
    {     
        foreach(JointDataGatherer jointDataGatherer in JointDataGatheres)
        { 
            JointData jointData = new JointData();
            
            jointData.jointMotionVctor = jointDataGatherer.GetJointMotionVector();
            jointData.jointVelocity = jointDataGatherer.GetJointVelocity();
            jointData.jointName = jointDataGatherer.GetJointName();
            jointData.jointType = jointDataGatherer.GetJointType();
            jointData.jointRegion = jointDataGatherer.GetJointRegion();

            dataFrame.JointsData.Add(jointData);        
        }    
    }

    private void Save()
    { 
        Debug.Log("Saveing starting!");
        FileStream fs = CreateFile(savePath);
        WriteCSVHeader(fs);
        int i = 0;
        foreach(DataFrame df in dataFrames)
        {
            i++;
            Debug.Log("Saveing data frame nr:" + i);
            WriteDataFrame(fs,df);
        }
        fs.Close();
        Debug.Log("Saveing completed!");
    }

    private FileStream CreateFile(string path) 
    {
        Debug.Log("Creating file at: " + path);
        FileInfo fileInfo = new FileInfo(path);
        Directory.CreateDirectory(fileInfo.DirectoryName);
        return File.Create(path + ".csv");
    }

    private void WriteToStream(FileStream stream, string text) 
    {
        stream.Write(new System.Text.UTF8Encoding(true).GetBytes(text));
    }

    private void WriteDataFrame(FileStream stream, DataFrame dataFrame)
    {
        WriteToStream(stream, dataFrame.dataFrameID + ";");
        WriteToStream(stream, dataFrame.frameTime + ";");
        WriteToStream(stream, dataFrame.engineFramecount + ";");
        foreach(JointData jd in dataFrame.JointsData)
        {
            WriteJointData(stream, jd);
        }
        WriteToStream(stream, "\n");
    }

    private void WriteJointData(FileStream stream, JointData jointData)
    {
        WriteToStream(stream, jointData.jointName+";");
        WriteToStream(stream, jointData.jointType+";");
        WriteToStream(stream, jointData.jointRegion+";");
        WriteToStream(stream, jointData.jointVelocity+";");
        WriteToStream(stream, jointData.jointMotionVctor.x + ";");
        WriteToStream(stream, jointData.jointMotionVctor.y + ";");
        WriteToStream(stream, jointData.jointMotionVctor.z + ";");
    }
    private void WriteCSVHeader(FileStream stream)
    {
        WriteToStream(stream, "Data Nr;");
        WriteToStream(stream, "Frame Time;");
        WriteToStream(stream, "Frame Nr;");
        foreach (JointData jd in dataFrames[0].JointsData)
        {
            WriteCSVJointHeader(stream);
        }
        WriteToStream(stream, "\n");

    }
    private void WriteCSVJointHeader(FileStream stream) 
    {
        WriteToStream(stream, "Joint Name;");
        WriteToStream(stream, "Joint Type;");
        WriteToStream(stream, "Joint Region;");
        WriteToStream(stream, "Joint Velocity;");
        WriteToStream(stream, "Joint Motion Vector X;");
        WriteToStream(stream, "Joint Motion Vector Y;");
        WriteToStream(stream, "Joint Motion Vector Z;");
    }

    private void Clear()
    {
        Debug.Log("Clearing starting!");
        dataFrames.Clear();
        Debug.Log("Clearing completed!");
    }
}
