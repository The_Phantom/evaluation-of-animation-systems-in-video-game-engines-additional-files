using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class JointDataGatherer : MonoBehaviour
{
    private Vector3 lastPosition = Vector3.zero;

    private Vector3 jointMotionVector = Vector3.zero;

    private float jointVelocity = 0;

    private string jointName = "";

    [SerializeField]
    private int jointType = 0; //0-Primary 1-Secondary
    [SerializeField]
    private int jointRegion = 0; //0-Other/Unspecify 1-LeftLeg 2-RightLeg 3-LeftArm 4-RightArm 5-Spine

    void Start()
    {
        jointName = this.gameObject.name;

        lastPosition = this.transform.position;
    }

    void Update()
    {
        CalculateData();
    }

    private void CalculateData()
    {
        jointMotionVector = (this.transform.position - lastPosition)*1000;//convert to mm

        lastPosition = this.transform.position;

        jointVelocity = jointMotionVector.magnitude/(Time.deltaTime*1000);
    }

    public Vector3 GetJointMotionVector() {
    
        return jointMotionVector;
    }

    public float GetJointVelocity()
    { 
        return jointVelocity;
    }

    public string GetJointName() 
    { 
        return jointName; 
    }

    public int GetJointType()
    { 
        return jointType;
    }

    public int GetJointRegion() {
        return jointRegion;
    }
}
