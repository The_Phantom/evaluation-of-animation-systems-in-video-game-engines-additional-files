#if ! UNITY_EDITOR
#pragma warning disable CS0618 // Type or member is obsolete (for MixerState in Animancer Lite).
#endif
#pragma warning disable CS0649 // Field is never assigned to, and will always have its default value.

using Animancer;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AnimancerComponent))]
public sealed class A_Script: MonoBehaviour
{

    [Header("System Setup")]
    [SerializeField] 
    private Animancer.AnimancerComponent _Animancer;
    [SerializeField] 
    private Animancer.MixerTransition2D  _Movment;
    [SerializeField]
    private DataManager dataManager;
    [SerializeField]
    private float segmentTime = 5.0f;
    [SerializeField]
    private float transitionTime = 0.5f;

    [Header("Controlls")]
    [SerializeField]
    private Scenario currentScenario = Scenario.Start_Walk_Stop;
    [SerializeField]
    private bool RecordData = false;

    [Space]
    [SerializeField]
    private bool PlayScenario = false;

    private bool CurrentlyPlaying = false;

    float currentSpeed = 0.0f;
    float currentDirection = 0.0f;

    private void OnEnable()
    {
        _Animancer.Play(_Movment);
    }

    void Update() 
    {
        if(PlayScenario) {
            PlayScenario = false;
            if(CurrentlyPlaying) {
                Debug.Log("Scenario is already being playied!");
            } else {
                Debug.Log("Playing choosen scenario.");
                StartCoroutine(ScenarioController());
            }
        }
    }

    enum Scenario {
        Start_Walk_Stop,
        Start_Walk_Run_Stop,
        Start_Walk_Run_Walk_Stop,
        Start_LStrafeRun_RStrafeRun_Run_Stop,
        Start_LStrafeWalk_RStrafeWalk_Walk_Stop,
        Start_LStrafeWalk_LStrafeRun_RStrafeWalk_RStrafeRun_Stop,
        Start_SpeedUpWalk_Walk_SpeedUpRun_Run_Stop
    }

    IEnumerator SegmentChange(float targetSpeed, float targetDirection) {
        float elapsedTime = 0.0f;

        float startingSpeed = currentSpeed;
        float startingDirection = currentDirection;

        while(elapsedTime <= segmentTime) {
            elapsedTime += Time.deltaTime;

            currentSpeed = Mathf.Lerp(startingSpeed, targetSpeed, elapsedTime / transitionTime);
            currentDirection = Mathf.Lerp(startingDirection, targetDirection, elapsedTime / transitionTime);

            _Movment.State.Parameter = new Vector2(currentSpeed, currentDirection);

            yield return null;
        }
    }

    IEnumerator ScenarioController() {
        CurrentlyPlaying = true;
        if(RecordData && dataManager) {
            dataManager.record = true;
        }
        switch(currentScenario) {
            case Scenario.Start_Walk_Stop: {
                    yield return SegmentChange(1.528f, 0.0f); //Start to Walk
                    yield return SegmentChange(0.0f, 0.0f); //Walk to Stop
                }
                break;
            case Scenario.Start_Walk_Run_Stop: {
                    yield return SegmentChange(1.528f, 0.0f); //Start to Walk
                    yield return SegmentChange(3.802f, 0.0f); //Walk to Run
                    yield return SegmentChange(0.0f, 0.0f); //Run to Stop
                }
                break;
            case Scenario.Start_Walk_Run_Walk_Stop: {
                    yield return SegmentChange(1.528f, 0.0f); //Start to Walk
                    yield return SegmentChange(3.802f, 0.0f); //Walk to Run
                    yield return SegmentChange(1.528f, 0.0f); //Run to Walk
                    yield return SegmentChange(0.0f, 0.0f); //Walk to Stop
                }
                break;
            case Scenario.Start_LStrafeRun_RStrafeRun_Run_Stop: {
                    yield return SegmentChange(3.526f, -2.071f); //Start to LStrafeRun
                    yield return SegmentChange(3.526f, 2.073f); //LStrafeRun to RStrafeRun
                    yield return SegmentChange(3.802f, 0.0f); //RStrafeRun to Run
                    yield return SegmentChange(0.0f, 0.0f); //Run to Stop
                }
                break;
            case Scenario.Start_LStrafeWalk_RStrafeWalk_Walk_Stop: {
                    yield return SegmentChange(1.269f, -0.946f); //Start to LStrafeWalk
                    yield return SegmentChange(1.268f, 0.946f); //LStrafeWalk to RStrafeWalk
                    yield return SegmentChange(1.528f, 0.0f); //RStrafeWalk to Walk
                    yield return SegmentChange(0.0f, 0.0f); //Walk to Stop
                }
                break;
            case Scenario.Start_LStrafeWalk_LStrafeRun_RStrafeWalk_RStrafeRun_Stop: {
                    yield return SegmentChange(1.269f, -0.946f); //Start to LStrafeWalk
                    yield return SegmentChange(3.526f, -2.071f); //LStrafeWalk to LStrafeRun
                    yield return SegmentChange(1.268f, 0.946f); //LStrafeRun to RStrafeWalk
                    yield return SegmentChange(3.526f, 2.073f); //RStrafeWalk to RStrafeRun
                    yield return SegmentChange(0.0f, 0.0f); //RStrafeRun to Stop
                }
                break;
            case Scenario.Start_SpeedUpWalk_Walk_SpeedUpRun_Run_Stop: {
                    yield return SegmentChange(0.382f, 0.0f);//1/4
                    yield return SegmentChange(0.764f, 0.0f);//1/2
                    yield return SegmentChange(1.146f, 0.0f);//3/4
                    yield return SegmentChange(1.528f, 0.0f);//1
                    yield return SegmentChange(1.901f, 0.0f);//1/2
                    yield return SegmentChange(2.851f, 0.0f);
                    yield return SegmentChange(3.802f, 0.0f);
                    yield return SegmentChange(0.0f, 0.0f);
                }
                break;
            default: {
                    yield return SegmentChange(0.0f, 0.0f); //Anything to Stop
                }
                break;
        }
        Debug.Log("Scenario has ended!");
        CurrentlyPlaying = false;
        if(RecordData && dataManager) {
            dataManager.record = false;
        }
    }
}

