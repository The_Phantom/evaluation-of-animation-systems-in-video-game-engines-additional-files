using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataFrame
{
    public int dataFrameID;
    public int engineFramecount;
    public float frameTime;
    public List<JointData> JointsData;
}

public struct JointData
{
    public Vector3 jointMotionVctor;
    public float jointVelocity;
    public string jointName;
    public int jointType;
    public int jointRegion;
}