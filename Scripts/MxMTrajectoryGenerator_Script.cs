﻿using System;
using UnityEngine;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;

namespace MxM
{
    [RequireComponent(typeof(MxMAnimator))]
    public class MxMTrajectoryGenerator_Script: MxMTrajectoryGeneratorBase
    {
        [Header("Motion Settings")]
        [SerializeField] private float m_posBias = 15f;
        [SerializeField] private float m_dirBias = 10f;
                
        private bool m_hasInputThisFrame;
        private NativeArray<float3> m_newTrajPositions;

        public Vector3 MotionVector { get; set; } = Vector3.zero;

        protected override void Setup(float[] a_predictionTimes) { }

        public void Update()
        {
            if (p_animator.updateMode != AnimatorUpdateMode.AnimatePhysics)
            {
                UpdatePastTrajectory(Time.deltaTime);
            }
        }

        protected override void UpdatePrediction(float a_deltaTime)
        {
            if (p_trajPositions.Length == 0 || p_trajFacingAngles.Length == 0)
                return;
            
            Vector3 desiredLinearVelocity = CalculateDesiredLinearVelocity(); 

            Vector3 desiredLinearDisplacement = desiredLinearVelocity / p_sampleRate;

            float desiredOrientation = 0f;

            if (desiredLinearDisplacement.sqrMagnitude > 0.0001f )
            {
                desiredOrientation = Mathf.Atan2(desiredLinearDisplacement.x,
                    desiredLinearDisplacement.z) * Mathf.Rad2Deg;
            }
            else
            {
                desiredOrientation = transform.rotation.eulerAngles.y;
            }
            var trajectoryGenerateJob = new TrajectoryGeneratorJob()
            {
                TrajectoryPositions = p_trajPositions,
                TrajectoryRotations = p_trajFacingAngles,
                NewTrajectoryPositions = m_newTrajPositions,
                DesiredLinearDisplacement = desiredLinearDisplacement,
                DesiredOrientation = desiredOrientation,
                MoveRate = m_posBias * a_deltaTime,
                TurnRate = m_dirBias * a_deltaTime
            };

            p_trajectoryGenerateJobHandle = trajectoryGenerateJob.Schedule();
        }

        private Vector3 CalculateDesiredLinearVelocity()
        {
            if (MotionVector.sqrMagnitude > 0.001f)
            {
                m_hasInputThisFrame = true;

                return MotionVector;
            }
            else
            {
                m_hasInputThisFrame = false;
            }

            //No input so just return zero
            return Vector3.zero;
        }        

        protected override void InitializeNativeData()
        {
            base.InitializeNativeData();

            m_newTrajPositions = new NativeArray<float3>(p_trajectoryIterations,
                Allocator.Persistent, NativeArrayOptions.ClearMemory);
        }

        protected override void DisposeNativeData()
        {
            base.DisposeNativeData();

            if (m_newTrajPositions.IsCreated)
                m_newTrajPositions.Dispose();
        }

        public override bool HasMovementInput()
        {
            return m_hasInputThisFrame;
        }    
    }
}